﻿using UnityEngine;

public class TeleportHandler : MonoBehaviour
{
	public Transform targetTransform;
	
	private void OnCollisionEnter2D(Collision2D collision)
	{
		Teleport(collision.gameObject);
	}

	private void Teleport(GameObject objectToTeleport)
	{
		objectToTeleport.transform.position = targetTransform.position;
	}
}
