﻿using System.Linq;
using UnityEngine;

public class PlayerJumpAttackHandler : MonoBehaviour
{
	private FoxAgent fox;

	void Start ()
	{
		fox = GetComponent<FoxAgent>();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		var enemy = collision.gameObject.GetComponent<Enemy>();
		if (fox.IsJumping 
			&& enemy != null 
			&& collision.contacts.Any(c => Mathf.Approximately(c.normal.y, 1.0f)))
		{
			Destroy(enemy.gameObject);
		}
	}
}
