﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeHandler : MonoBehaviour
{
	public string sceneName;

	void OnCollisionEnter2D(Collision2D collision)
	{
		var fox = collision.gameObject.GetComponent<FoxAgent>();
		if(fox != null)
		{
			SceneManager.LoadScene(sceneName);
		}
	}
}
