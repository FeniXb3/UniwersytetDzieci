﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class PlayerRespawnHandler : MonoBehaviour
{
	private Vector3 initialPosition;
	public bool left = true;
	public bool right = true;
	public bool top = false;
	public bool bottom =true;

	void Start ()
	{
		initialPosition = transform.position;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		var enemy = collision.gameObject.GetComponent<Enemy>();
		if (enemy != null && IsRespawnCollision(collision))
		{
			Respawn();
		}
	}

	private bool IsRespawnCollision(Collision2D collision)
	{
		return collision.contacts.Any(c =>
			(Mathf.Approximately(c.normal.x, -1f) && left)
			|| (Mathf.Approximately(c.normal.x, 1f) && right)
			|| (Mathf.Approximately(c.normal.y, -1f) && bottom)
			|| (Mathf.Approximately(c.normal.x, 1f) && top)
			);
	}

	private void Respawn()
	{
		transform.position = initialPosition;
	}
}
